#include <iostream>

class Animal
{
public:
    virtual void Voice() = 0;
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meaw...";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moooooo!";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Cow();

    std::cout << "Here's our animals:\n";
    for (auto& a : animals)
    {
        a->Voice();
        std::cout << '\n';
    }

    for (auto& a : animals)
    {
       delete a;
    }

    return 0;
}
